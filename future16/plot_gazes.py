import cPickle as pickle
import numpy as np
import matplotlib.pyplot as plt
import cv2
import scipy.interpolate

import sys
sys.path.append('..')
import smoothpose

def pose_interpolator(ts, rvecs, tvecs):
    quats = smoothpose.Q.from_rvec(rvecs)
    pinterp = smoothpose.interpolate_pose(ts, quats, tvecs)
    def interp(ts):
        q, t = pinterp(ts)
        return smoothpose.Q.to_rvec(q), t
    return interp

def invert_pose(rvec, tvec):
    R = cv2.Rodrigues(rvec)[0].T
    return cv2.Rodrigues(R)[0], np.dot(R, -tvec)


def transform_points(world, rvec, tvec):
    angle = np.linalg.norm(rvec)
    if angle > 1e-6:
        axis = (rvec/angle).reshape(3, 1)
        x, y, z = axis.reshape(-1)
        R = np.array((((0.0, -z, y), (z, 0.0, -x), (-y, x, 0.0))))
        R = np.cos(angle)*np.identity(3) + np.sin(angle)*R + (1 - np.cos(angle))*np.dot(axis, axis.T)
        world = np.inner(R, world).T
    tvec = tvec.reshape(-1)
    return world + tvec

def main(pupil_data, video, video_timestamps):
    pupil_data = pickle.load(open(pupil_data))
    
    video_timestamps = np.load(video_timestamps)
    frame_to_pupil = scipy.interpolate.interp1d(np.arange(len(video_timestamps)), video_timestamps)
    pupil_to_frame = scipy.interpolate.interp1d(frame_to_pupil.y, frame_to_pupil.x)
    
    ts = []
    poses = []
    gaze_directions = []
    for row in pupil_data['gaze_positions']:
        ts.append(row['timestamp'])
        pose = row['world']['camera_pose']
        poses.append(pose)
        gaze_direction = row['world']['eye_in_head']
        gaze_directions.append(gaze_direction)
        #continue

    
    cap = cv2.VideoCapture(video)
    cap.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, int(pupil_to_frame(6000)))
    gaze_interp = scipy.interpolate.interp1d(ts, gaze_directions, axis=0)
    pose_interp = pose_interpolator(ts, *zip(*poses))
    
    data = []
    #while True:
    for row in pupil_data['gaze_positions']:
        
        #ret, frame = cap.read()
        #if not ret: break
        #ts = frame_to_pupil(cap.get(cv2.cv.CV_CAP_PROP_POS_FRAMES))
    
        ts = row['timestamp']
        
        
        try:
            pose = pose_interp(ts)
            gaze_direction = gaze_interp(ts)*10.0
        except ValueError:
            continue

        pose = invert_pose(*pose)
        origin = np.array([0, 0, 0], dtype=np.float)
        nose = np.array([0, 0, 0.05], dtype=np.float)
        
        feats = np.array([origin, nose, gaze_direction])
        
        feats = transform_points(feats, *pose)
        data.append((ts, feats[-1]))
        continue
        plt.clf()
        plt.subplot(1,2,1)
        plt.xlim(-0.2, 0.2)
        plt.ylim(-0.2, 0.2)

        plt.plot(feats[[0,1],0], feats[[0,1],2], 'o-')
        plt.plot(feats[[0,2],0], feats[[0,2],2], '-')
        # NOTE: Opencv uses y-down coordinate system
        #plt.plot(feats[[0,1],0], -feats[[0,1],1], 'o-')
        #plt.plot(feats[[0,2],0], -feats[[0,2],1], '-')
        plt.subplot(1,2,2)
        plt.imshow(frame)
        plt.draw()
        plt.pause(0.01)
    ts, direction = map(np.array, zip(*data))
    heading = np.arctan2(direction[:,0], direction[:,2])
    #heading = direction[:,0]
    dt = np.gradient(ts)
    from scipy.signal import medfilt
    dh = np.gradient(heading)/dt
    #plt.plot(ts, medfilt(np.degrees(dh), 101), '.', alpha=0.1)
    #plt.plot(ts, np.degrees(dh), '.', alpha=0.1)
    plt.plot(ts, np.degrees(heading), '.', alpha=1.0)
    plt.show()

if __name__ == '__main__':
    import argh; argh.dispatch_command(main)
