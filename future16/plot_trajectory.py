import numpy as np
import matplotlib.pyplot as plt
#import cPickle as pickle
import pickle
import sys

sys.path.append('..') # HACK!
import smoothpose
import cv2

def transform_points(world, rvec, tvec):
    angle = np.linalg.norm(rvec)
    if angle > 1e-6:
        axis = (rvec/angle).reshape(3, 1)
        x, y, z = axis.reshape(-1)
        R = np.array((((0.0, -z, y), (z, 0.0, -x), (-y, x, 0.0))))
        R = np.cos(angle)*np.identity(3) + np.sin(angle)*R + (1 - np.cos(angle))*np.dot(axis, axis.T)
        world = np.inner(R, world).T
    tvec = tvec.reshape(-1)
    return world + tvec

def camera_homography(cm_src, pose_src, cm_dst, pose_dst):
    # This is a bit stupid way of finding the homography,
    # but there doesn't seem to be a function for this in OpenCV
    # and I don't feel like linear algebra ATM.
    # This is probably stupid
    
    test_points = np.array([
        [-1, -1, 2],
        [-1, 1, 2],
        [1, 1, 2],
        [1, -1, 2],
        ], dtype=np.float32)
    
    src_r, src_t = pose_src
    dst_r, dst_t = pose_dst
    
    src_R = cv2.Rodrigues(src_r)[0]
    dst_R = cv2.Rodrigues(dst_r)[0]
    
    rel_R = np.dot(src_R, dst_R.T)
    rel_t = -np.dot(rel_R, dst_t) + src_t
    rel_r = cv2.Rodrigues(rel_R)[0]

    #s_src = cv2.projectPoints(test_points, src_r, src_t, cm_src, None)[0].reshape(-1, 2)
    #s_dst = cv2.projectPoints(test_points, dst_r, dst_t, cm_dst, None)[0].reshape(-1, 2)
    
    s_src = cv2.projectPoints(test_points, rel_r, rel_t, cm_src, None)[0].reshape(-1, 2)
    s_dst = cv2.projectPoints(test_points, np.zeros(3), np.zeros(3), cm_dst, None)[0].reshape(-1, 2)


    H = cv2.getPerspectiveTransform(s_src, s_dst)
    return H

# Reimplementation of OpenCV 3's fisheye undistortion.
def undistort_points_fisheye(distorted, K, D, P):
    undistorted = np.ones((len(distorted), 3), dtype=distorted.dtype)
    K = np.asarray(K)
    f = np.array([K[0,0], K[1,1]])
    c = np.array([K[0,2], K[1,2]])
    D = np.asarray(D)
    P = np.asarray(P)
    
    for i in range(len(distorted)):
        pw = (distorted[i] - c)/f
        theta_d = np.linalg.norm(pw)
        # This is in the original implementation, although
        # theta_d is clearly always positive
        theta_d = np.clip(theta_d, -np.pi/2.0, np.pi/2.0)
        scale = 1.0
        if theta_d > 1e-8:
            theta = theta_d
            for j in range(10):
                theta = theta_d/(1 + np.sum(D*np.power(theta, [2,4,6,8])))
            scale = np.tan(theta)/theta_d
        undistorted[i,:2] = pw*scale
    
    undistorted = np.inner(undistorted, P)
    undistorted /= undistorted[:,-1]
    return undistorted[:,:2]
    
def pixel_to_angle(pixel, cm):
    f = np.array([cm[0,0], cm[1,1]])
    c = np.array([cm[0,2], cm[1,2]])

    return np.arctan((pixel - c)/f)

import scipy.interpolate
import yaml
def main(camera_spec, origin_pose, poses, marker_points, videofile, videomap, gaze=None, gaze_ts=None):
    if gaze and gaze_ts:
        gaze = pickle.load(open(gaze, 'rb'))
        frame_gaze_ts = np.load(gaze_ts)
        gaze_ts = []
        gaze_pos = []
        for g in gaze['gaze_positions']:
            gaze_ts.append(g['timestamp'])
            gaze_pos.append(g['norm_pos'])
        gaze_ts_to_idx = scipy.interpolate.interp1d(frame_gaze_ts, np.arange(len(frame_gaze_ts)), bounds_error=False)
        gaze_interp = scipy.interpolate.interp1d(gaze_ts_to_idx(gaze_ts), gaze_pos, axis=0)
    else:
        gaze_interp = None
    camera = pickle.load(open(camera_spec, 'rb'), encoding='bytes')
    videomap = pickle.load(open(videomap, 'rb'), encoding='bytes')
    #cm, cd, resolution = camera['camera_matrix'], camera['dist_coefs'], camera['resolution']
    cm = videomap[b'rect_camera_matrix']
    cd = None
    resolution = videomap[b'resolution']
    poses = pickle.load(open(poses, 'rb'), encoding='bytes')
    
    times = []
    tvecs = []
    rvecs = []
    for i, pose in enumerate(poses):
        ts = pose[b'time']
        times.append(pose[b'time'])
        rvecs.append(pose[b'rvec'].reshape(-1))
        tvecs.append(pose[b'tvec'].reshape(-1))
        #if ts > 60 + 60*5: break
    
    quats = smoothpose.Q.from_rvec(rvecs)
    
    #print ref_rvec
    #print ref_tvec
    
    interp = smoothpose.interpolate_pose(times, quats, np.array(tvecs))
    #interp = smoothpose.smooth_pose(times, quats, np.array(tvecs))
    #iq, it = interp(times)
    #ax = plt.subplot(3,1,1); plt.plot(times, np.array(quats)[:,2]); plt.plot(times, iq[:,2])
    #plt.subplot(3,1,2, sharex=ax); plt.plot(times, np.array(tvecs)[:,1]); plt.plot(times, it[:,1])
    #plt.subplot(3,1,3, sharex=ax); plt.plot(times, np.array(tvecs)[:,2]); plt.plot(times, it[:,2])
    #plt.show()
    
    mean_t = np.mean(tvecs)

    marker_points = pickle.load(open(marker_points, 'rb'), encoding='bytes')

    """
    _canvas = np.zeros((resolution[1], resolution[0], 3), dtype=np.uint8)
    if videofile is not None:
        cap = cv2.VideoCapture(videofile)
        map1, map2 = pickle.load(open(videomap))['rect_map']
        def get_canvas(t):
            cap.set(cv2.cv.CV_CAP_PROP_POS_MSEC, t*1000)
            cap.read(_canvas)
            cv2.remap(_canvas, map1, map2, cv2.INTER_LINEAR, dst=_canvas)
            return _canvas
    else:
        def get_canvas(ts):
            _canvas[:] = 255
            return _canvas
    """
    
    canvas = np.zeros((resolution[1], resolution[0], 3), dtype=np.uint8)
    cap = cv2.VideoCapture(videofile)
    #cap.set(cv2.cv.CV_CAP_PROP_POS_MSEC, times[0]*1000)
    #cap.set(cv2.CAP_PROP_POS_MSEC, (20.0*60)*1000)
    fps = float(cap.get(cv2.CAP_PROP_FPS))
    map1, map2 = videomap[b'rect_map']
    K, D = videomap[b'camera_matrix'], videomap[b'dist_coefs']
    
    origin_pose = yaml.load(open(origin_pose))
    ref_rvec = np.array(origin_pose['rvec'])
    ref_tvec = np.array(origin_pose['tvec'])
    
    ref_quat, ref_tvec = interp((1*60 + 13))
    ref_rvec = smoothpose.Q.to_rvec(ref_quat[0])
    
    wide_cm = cm.copy()
    #wide_cm[0,0] /= 1.5
    #wide_cm[1,1] /= 1.5
    #wide_cm[0,-1] *= 1.5
    #wide_cm[1,-1] *= 1.5
    #wide_resolution = (resolution[0]*2, resolution[1]*2)
    wide_resolution = resolution

    while True:
        #canvas = get_canvas(ts)
        cap.read(canvas)
        ts = cap.get(cv2.CAP_PROP_POS_MSEC)/1000.0
        frame_i = cap.get(cv2.CAP_PROP_POS_FRAMES)
        if gaze_interp:
            try:
                gaze = gaze_interp(frame_i)
                x, y = gaze
                x *= resolution[0]
                y = 1 - y
                y *= resolution[1]
                cv2.circle(canvas, center=(int(x), int(y)), thickness=2, radius=5, color=(0, 0, 255))
            except ValueError:
                continue
            cv2.remap(canvas, map1, map2, cv2.INTER_LINEAR, dst=canvas)
        
            gaze *= resolution
            gaze = undistort_points_fisheye(gaze.reshape(1, 2), K, D, cm)
            gaze[:,1] = resolution[1] - gaze[:,1]
            cv2.circle(canvas, center=tuple(np.int0(gaze)[0]), thickness=2, radius=10, color=(0, 0, 255))

        try:
            quat, tvec = interp(ts)
        except ValueError:
            continue
        rvec = smoothpose.Q.to_rvec(quat)
        rendered = set()
        for mid, world in marker_points.items():
            #est = cv2.projectPoints(world, rvec, tvec, cm, cd)[0]#.reshape(-1, 2)
            est = cv2.projectPoints(world, rvec, tvec, cm, cd)[0]#.reshape(-1, 2)
            
            #est = np.array([mba.project_point(p, rvec, tvec, cm) for p in world])
            depths = transform_points(world, rvec, tvec)[:,2]
            if np.all(depths > 1e-2):
                center = np.int0(np.mean(est, axis=0))
                cv2.putText(canvas,
                        str(mid),
                        tuple(center[0]),
                        fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                        fontScale=1.0,
                        color=(0, 255, 0)
                        )
                cv2.polylines(canvas, np.int0([est]), color=(0, 0, 255), isClosed=False, thickness=2)
            #if mid in frame:
            #    cv2.polylines(canvas, np.int0([frame[mid].reshape(-1, 1, 2)]),
            #            color=(0, 0, 255), isClosed=False, thickness=1)
            #for point, depth in zip(est, depths):
            #    print depth
            #    if depth > 0:
            #        color = (0, 255, 0)
            #    else:
            #        color = (0, 0, 255)
            #    point = tuple(np.int0(point))
            #    cv2.polylines(canvas, np.int0([est]), color=color, isClosed=False, thickness=2)
            #    #cv2.circle(canvas, center=point, radius=2, color=color)

        H = camera_homography(cm, (rvec, tvec), wide_cm, (ref_rvec, ref_tvec))
        canvas2 = cv2.warpPerspective(canvas, H, wide_resolution)
        cv2.imshow("frame", canvas2)
        cv2.waitKey(1)


import argh
argh.dispatch_command(main)
