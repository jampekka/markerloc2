#!/bin/bash
VIDEO=$1
python3 ./plot_trajectory.py camera_params_rect.pickle origin_pose.yaml $VIDEO.poses.pickle $VIDEO.marker_positions.pickle $VIDEO  <( gunzip < ../camera_models/pupil.fisheye.crop.720p.pickle.gz)
